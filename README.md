
## Saving Wallet 

To be able to run the project, please follow the steps bellow :

- After clone the project form Bitbucket from the following link : git clone https://hudamajed@bitbucket.org/hudamajed/saving-wallet.git
- cd to the project and run **composer install**.
- Apply **php artisan key:generate**
- Apply **php artisan migrate** command to migrate the database. 
- Apply **php artisan db:seed** to fill some data into table.
- Edit your .env file 
- Apply the command **php artisan serve** to run the project

**Note : the login window is the same used for the User/Admin**

**Admin credential :**

Email: admin@admin.com

Password: 123456

