<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Salary',
            'type' => Category::$TYPE_INCOME,
        ]);

        Category::create([
            'name' => 'Bonuses',
            'type' => Category::$TYPE_INCOME,
        ]);

        Category::create([
            'name' => 'Overtime',
            'type' => Category::$TYPE_INCOME,
        ]);

        Category::create([
            'name' => 'Food & Drinks',
            'type' => Category::$TYPE_EXPENSE,
        ]);

        Category::create([
            'name' => 'Shopping',
            'type' => Category::$TYPE_EXPENSE,
        ]);

        Category::create([
            'name' => 'Housing',
            'type' => Category::$TYPE_EXPENSE,
        ]);

        Category::create([
            'name' => 'Transportation',
            'type' => Category::$TYPE_EXPENSE,
        ]);
    }
}
