<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Users List') }}
        </h2>
    </x-slot>
    <div class="p-5">
        <table>
            <tr>
                <th> {{ __('Name') }}</th>
                <th> {{ __('Email') }}</th>
                <th> {{ __('Phone') }}</th>
                <th> {{ __('Birthdate') }}</th>
                <th> {{ __('Total income') }}</th>
                <th> {{ __('Total expenses') }}</th>
                <th> {{ __('Wallet balance') }}</th>
                <th> {{ __('Registered date') }}</th>
            </tr>

            @foreach($users as $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone}}</td>
                    <td>{{$user->birthdate}}</td>
                    <td>{{$user->totalIncome()}}</td>
                    <td>{{$user->totalExpenses()}}</td>
                    <td>{{$user->walletBalance()}}</td>
                    <td>{{$user->created_at}}</td>
                </tr>
            @endforeach
        </table>
    </div>

</x-app-layout>
