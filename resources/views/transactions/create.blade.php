
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add New Transaction') }}
        </h2>
    </x-slot>

    <x-auth-card>
        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 font-weight-bold">
                Add New Transaction!
            </div>
        </div>
        <x-slot name="logo"></x-slot>
        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('add-transaction') }}">
            @csrf
            <!-- Category -->
            <div class="mt-4">
                <x-label for="category" :value="__('Category')" />

                <select id="category" class="block mt-1 w-full" name="category">
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>

            <a data-toggle="modal" data-target="#mediumModal" class="custom-btn" >Add Your own category</a>

            <!-- Amount -->
            <div class="mt-4">
                <x-label for="amount" :value="__('Amount')" />
                <x-input id="amount" class="block mt-1 w-full" type="number" name="amount" min="0" :value="old('amount')" required />
            </div>

            <!-- Note -->
            <div class="mt-4">
                <x-label for="note" :value="__('Note')" />
                <x-input id="note" class="block mt-1 w-full" type="text" name="note" :value="old('Note')" />
            </div>

            <div class="flex items-center justify-end mt-4">
                <x-button class="ml-4">
                    {{ __('Submit') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>

</x-app-layout>

<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" id="mediumBody">
                <form method="POST" action="{{ route('add-category') }}">
                    @csrf
                    <div class="mt-4">
                        <x-label for="name" :value="__('Name')" />
                        <x-input id="name" class="block mt-1 w-full" type="text" name="name" min="0" :value="old('name')" required />
                    </div>
                    <div class="mt-4">
                        <x-label for="type" :value="__('Type')" />
                        <select id="type" class="block mt-1 w-full" name="type">
                            <option value="income">Income</option>
                            <option value="expense">Expense</option>
                        </select>
                    </div>
                    <x-button class="ml-4 custom-submit" >
                        {{ __('Submit') }}
                    </x-button>
                </form>
            </div>
        </div>
    </div>
</div>
