<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add New Transaction') }}
        </h2>
    </x-slot>
    <div class="p-5">
        <table>
            <tr>
                <th> {{ __('ID') }}</th>
                <th> {{ __('Type') }}</th>
                <th> {{ __('Category') }}</th>
                <th> {{ __('Amount') }}</th>
                <th> {{ __('Note') }}</th>
                <th> {{ __('Created At') }}</th>
            </tr>
            @foreach($transactions as $transaction)
                <tr>
                    <td>{{$transaction->id}}</td>
                    <td>{{$transaction->category->type == 'income' ? 'Income': 'Expense'}}</td>
                    <td>{{$transaction->category->name}}</td>
                    <td>{{$transaction->amount}}</td>
                    <td>{{$transaction->note}}</td>
                    <td>{{$transaction->created_at}}</td>
                </tr>
            @endforeach

        </table>
        <div class="p-5">
            <div class="row"> Summary :</div>
            <div class="row"> Total of income : {{$total_incomes}} </div>
            <div class="row"> Total of expenses : {{$total_expenses}} </div>
            <div class="row"> Wallet balance : {{$wallet_balance}} <br></div>
        </div>
    </div>

</x-app-layout>
