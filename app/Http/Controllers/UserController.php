<?php

namespace App\Http\Controllers;

use App\Models\Roles;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * List all transactions for current user
     */
    public function index()
    {
        $users = User::where('role_id', Roles::$TYPE_USER)->get();

        return view('users.index', [
            'users' => $users,
        ]);
    }

}

