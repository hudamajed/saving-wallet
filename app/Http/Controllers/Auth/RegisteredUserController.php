<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Roles;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'max:255', 'unique:users'],
            'image' => ['required', 'mimes:jpeg,png,jpg,gif,svg', 'max:5120'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $profile_image = null ;
        if ($image = $request->file('image')) {
            $destination_path = 'image/';
            $profile_image = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destination_path, $profile_image);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'birthdate' => $request->birthdate,
            'role_id' => Roles::$TYPE_USER,
            'password' => Hash::make($request->password),
            'image' => $profile_image,
        ]);

        event(new Registered($user));

        Auth::login($user);

        if (Auth::user()->role_id == Roles::$TYPE_ADMIN){
            return redirect()->intended(RouteServiceProvider::HOME_ADMIN);
        }else{
            return redirect()->intended(RouteServiceProvider::HOME_USER);

        }
    }
}
