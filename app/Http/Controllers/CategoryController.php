<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required'],
        ]);

        Category::create([
            'name' => $request->name,
            'user_id' => Auth::user()->id,
            'type' => $request->type,
        ]);

        return redirect(route('transactions'));
    }
}
