<?php

namespace App\Http\Controllers;


use App\Models\Category;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class TransactionController extends Controller
{
    /**
     * List all transactions for current user
     */
    public function index()
    {
        $transactions = Transaction::where('user_id', Auth::user()->id)->get();
        $total_incomes = $this->getTotalIncomes();
        $total_expenses = $this->getTotalExpenses();
        $wallet_balance = $total_incomes - $total_expenses ;

        return view('transactions.index', [
            'transactions' => $transactions,
            'total_incomes' => $total_incomes,
            'total_expenses' => $total_expenses,
            'wallet_balance' => $wallet_balance,
        ]);
    }

    /**
     * Show the form for creating a new transaction.
     */
    public function create()
    {
        // Get categories list (pre-defined : user_id = null ) with user categories (user_id = auth id)
        $categories = Category::whereNull('user_id')
            ->orWhere('user_id', Auth::user()->id)
            ->get();

        return view('transactions.create', ['categories' => $categories]);
    }


    /**
     * Handle an incoming transactions.
     **/
    public function store(Request $request)
    {
        $wallet_balance = $this->getTotalIncomes() - $this->getTotalExpenses() ;

        $request->validate([
            'category' => ["required"],
            'amount' => ["required","integer"],
        ]);

        if (in_array($request->category , Category::getExpensesCategories())){
            $request->validate([
                'amount' => ["required","integer","max:$wallet_balance"],
            ]);
        }

        Transaction::create([
            'user_id' => Auth::user()->id,
            'category_id' => $request->category,
            'amount' => $request->amount,
            'note' => $request->note,
        ]);

        return redirect(route('transactions'));
    }

    public function getTotalIncomes(){

        return Transaction::whereHas('category', function ($query) {
            $query->where('type',Category::$TYPE_INCOME);
        })
        ->with('category')
        ->where('user_id', Auth::user()->id)
        ->sum('amount');

    }

    public function getTotalExpenses(){

        return Transaction::whereHas('category', function ($query) {
            $query->where('type',Category::$TYPE_EXPENSE);
        })
        ->with('category')
        ->where('user_id', Auth::user()->id)
        ->sum('amount');
    }

    /**
     * report
     */
    public function report()
    {
        $categories = Category::whereNull('user_id')
            ->orWhere('user_id', Auth::user()->id)
            ->pluck('id')
            ->toArray();

        $transactions = [];
        foreach ($categories as $key => $value) {
            $transactions [] = Transaction::where('user_id', Auth::user()->id)
                ->where('category_id', $value)
                ->sum('amount');
        }

        $categories_labels = Category::whereNull('user_id')
            ->orWhere('user_id', Auth::user()->id)
            ->pluck('name')
            ->toArray();

        return view('transactions.report')
            ->with('categories',json_encode($categories_labels,JSON_NUMERIC_CHECK))
            ->with('transactions',json_encode($transactions,JSON_NUMERIC_CHECK));

    }

}
