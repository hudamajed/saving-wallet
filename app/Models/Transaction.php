<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'category_id',
        'amount',
        'note',
    ];

    /**
     * Get the category associated with the transaction.
     */
    public function category()
    {
        return $this->hasOne(Category::class ,'id' ,'category_id');
    }
}
