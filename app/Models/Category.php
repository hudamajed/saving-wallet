<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    public static $TYPE_INCOME = 'income';
    public static $TYPE_EXPENSE = 'expense';

    protected $fillable = [
        'name',
        'type',
        'user_id',
    ];

    public static function getExpensesCategories(){
        return self::where('type', self::$TYPE_EXPENSE)->pluck('id')->toArray();
    }

    public static function getIncomeCategories(){
        return self::where('type', self::$TYPE_INCOME)->pluck('id')->toArray();
    }

}
