<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', [UserController::class, 'index'])->middleware(['auth','admin'])->name('dashboard');
Route::get('/add-transaction', [TransactionController::class, 'create'])->middleware(['auth','user'])->name('add-transaction');
Route::post('/add-transaction', [TransactionController::class, 'store'])->middleware(['auth','user'])->name('add-transaction');
Route::post('/add-category', [CategoryController::class, 'store'])->middleware(['auth','user'])->name('add-category');
Route::get('/transactions', [TransactionController::class, 'index'])->middleware(['auth','user'])->name('transactions');
Route::get('/reports', [TransactionController::class, 'report'])->middleware(['auth','user'])->name('report');

require __DIR__.'/auth.php';
